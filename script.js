var users = [];
var tableBody = document.getElementById("tbody");

  function createTable(tableData) {
  
    tableData.forEach(function(rowData) {
      var row = document.createElement('tr');
      row.id = rowData[0];
        row.innerHTML = "<th scope='row'>" + rowData[0] + "</th>"+
        "<td>" + rowData[1] + "</td><td>" + rowData[2] + "</td><td>"+ 
        "<button type='button' class='btn btn-primary'>Modifier</button>"+
        "<button type='button' class='btn btn-danger'>Supprimer</button>"+
        "</td>";
      tableBody.appendChild(row);
    });
  
    document.body.appendChild(tableBody);
  }

  function addUser() {
    var login = document.getElementById('recipient-login').value;
    var password = document.getElementById('recipient-password').value;
    users.push([users.length+1, login, password]);
    createTable([[users.length, login, password]]);
  }